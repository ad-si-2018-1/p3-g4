var express = require('express');             // módulo express
var app = express();                          // objeto express
var bodyParser = require('body-parser');      // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var path = require('path');                   // caminho de arquivos
var amqp = require('amqplib/callback_api');   // comunicação amqp

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var id_gen = 0; // Gerador de ID
var users = []; // Array de Usuários
var amqp_conn;
var amqp_ch;

/**
 * A primeira coisa que o servidor da aplicação faz é estabelece conexão com o servidor AMQP,
 *  antes que qualquer cliente se conecte
 */
//amqp.connect('amqp://zuydxjud:DyvBxs-zqRaHovmN7fS50MxzMrEn8YGi@wombat.rmq.cloudamqp.com/zuydxjud', function(err, conn) {
amqp.connect('amqp://localhost', function(err, conn) {
  if (err) {
    console.log('Erro ao iniciar conexao com servidor AMQP: ' + err);
  }
  conn.createChannel(function(err, ch) {
    amqp_conn = conn;
    amqp_ch = ch;
  });
});

/**
 * Realiza login gravando dados nos cookies
 */
app.post('/login', function (req, res) {
  res.cookie('nick', req.body.nome);

  if(req.body.canal && req.body.canal[0]!='#') {
    req.body.canal = '#'+req.body.canal;
  }

  res.cookie('canal', req.body.canal);
  res.cookie('servidor', req.body.servidor);
  res.redirect('/');
});

/**
 * Função para enviar comandos para o servidor AMQP
 *
 * @param {string} comando
 * @param {object} msg
 */
function enviarParaServidor (comando, msg) {
  msg = new Buffer(JSON.stringify(msg));
  
  amqp_ch.assertQueue(comando, {durable: false});
  amqp_ch.sendToQueue(comando, msg);
  console.log(" [app] Enviado %s", msg);  
}

/**
 * Função para receber retornos do servidor AMQP
 *
 * @param {*} id
 * @param {*} callback
 */
function receberDoServidor (id, callback) {
  amqp_ch.assertQueue("user_" + id, {durable: false});
  
  console.log(" [app] Aguardando mensagens de " + id);
  
  amqp_ch.consume("user_" + id, function(msg) {
    console.log(" [app] ID " + id + " Recebeu " + msg.content.toString());
    callback(id, JSON.parse(msg.content.toString()));
  }, {noAck: true});
}

app.use(function(req, res, next) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal) {
    if (!req.cookies.id) {
      req.cookies.id = id_gen++;
      res.cookie('id', req.cookies.id);
    }
    var usuario = users.find(function(u) {
      return u.id == req.cookies.id
    })
    
    if (!usuario) {
      // caso o usuário esteja sendo criado, envio um registro de conexão para o proxy IRC
      usuario = {
        id: req.cookies.id,
        servidor: req.cookies.servidor,
        nick: req.cookies.nick,
        canal: req.cookies.canal,
        cache: []
      };

      var target = 'registro_conexao';
      var msg = {
        id: usuario.id, 
        servidor: req.cookies.servidor,
        nick: req.cookies.nick, 
        canal: req.cookies.canal
      };
      
      usuario.id       = msg.id;
      usuario.servidor = msg.servidor;
      usuario.nick     = msg.nick;
      usuario.canal    = msg.canal;

      users.push(usuario);
      
      //Enviando registro de conexao para servidor AMQP
      enviarParaServidor(target, usuario);
    }
    next();
  } else {
    res.sendFile(path.join(__dirname, '/public/view/login.html'));
  }
});

/**
 * Rota /,
 * usada para registrar a conexão com o servidor IRC
 */
app.get('/', function (req, res) {
  //verifica se os dados do usuario estão salvos nos cookies do navegador, caso não esteja passa para login
  //if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
    var indice = users.findIndex(function(u) {
      return u.id == req.cookies.id
    })
    if (indice < 0) {
      res.end();
    }
    // Cria um cache de mensagens
    users[indice].cache = []; 
     
    //Se inscreve no servidor AMQP para receber mensagens
    receberDoServidor(users[indice].id, function (id_real, msg) {
        //Adiciona mensagem ao cache do usuário
        console.log("Mensagem adicionada ao cache do usuário " + users[indice].nick);
        users[indice].cache.push(msg);
    });

    res.sendFile(path.join(__dirname, '/public/view/index.html'));
  //} else {
  //  res.sendFile(path.join(__dirname, '/public/view/login.html'));
  //}
});

/** 
 * Obtém mensagens armazenadas em cache (via polling)
*/
app.get('/obter_mensagem/:timestamp', function (req, res) {
  var indice = users.findIndex(function(u) {
    return u.id == req.cookies.id
  })
  if (indice < 0) {
    res.end();
  }

  var response = users[indice].cache;
  //limpando o cache do usuario
  //users.splice(indice, 1);
  users[indice].cache = [];
  
  res.append('Content-type', 'application/json');
  res.send(response);
});

/**
 * Rota /gravar_mensagem,
 * usada para enviar mensagens para o servidor IRC
 */
app.post('/gravar_mensagem', function (req, res) {
  var indice = users.findIndex(function(u) {
    return u.id == req.cookies.id
  })
  if (indice < 0) {
    res.end();
  }
  // Adiciona mensagem enviada ao cache do usuário
  users[indice].cache.push(req.body);
  
  enviarParaServidor("gravar_mensagem", {
    id: req.cookies.id,
    nick: users[indice].nick,
    canal: users[indice].canal, 
    msg: req.body.msg
  });
  
  res.end();
});

/**
 * Inicia o servidor web na porta 3000
 */
app.listen(3000, function () {  
  console.log('Example app listening on port 3000!');  
});
