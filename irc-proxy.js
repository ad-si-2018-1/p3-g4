var irc = require('irc');
var amqp = require('amqplib/callback_api');

var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_clients = [];
var servidor_irc = 'Servidor IRC';

/**
 * A primeira coisa a se fazer é estabelece conexão com o servidor AMQP
 */
//amqp.connect('amqp://zuydxjud:DyvBxs-zqRaHovmN7fS50MxzMrEn8YGi@wombat.rmq.cloudamqp.com/zuydxjud', function(err, conn) {
amqp.connect('amqp://localhost', function(err, conn) {
  if (err) {
    console.log('Erro ao iniciar conexao com servidor AMQP e proxy IRC: ' + err);
  }

  conn.createChannel(function(err, ch) {
    amqp_conn = conn;
    amqp_ch = ch;
    inicializar();
  });
});

function inicializar() {
  receberDoCliente("registro_conexao", function (msg) {
    console.log('irc-proxy.js: recebeu registro de conexão');
    
    var id       = msg.id;
    var servidor = msg.servidor;
    var nick     = msg.nick;
    var canal    = msg.canal;
    
    var irc_client = new irc.Client(
      servidor, 
      nick,
      {channels: [canal]}
    );
    
    // adicionando Listener para o servidor IRC
    irc_client.addListener('error', function(message) {
      console.log('error: ', message);
    });

    irc_client.addListener('message' + canal, function (from, message) {
      console.log(from + ' => ' + canal + ': ' + message);
      
      enviarParaCliente(id, {
        "timestamp": Date.now(), 
        "nick": from,
        "msg": message
      });
    });
    /**
     * Mensagem do servidor informando que cliente foi registrado
     */
    irc_client.addListener('registered', function (message) {
      console.log("\"Ouvindo\" o evento MOTD " + message);
      
      enviarParaCliente(id, {
        "timestamp": Date.now(), 
        "nick": servidor_irc,
        "msg": message.args[1]
      });
    });
    /**
     * Mensagem do dia passada pelo servidor
     */
    irc_client.addListener('motd', function (message) {
      console.log("\"Ouvindo\" o evento MOTD " + message);
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": message
      });
    });
    /**
     * Servidor IRC retorna lista de nicks em um canal
     * Não está sendo enviado para o cliente, caso seja implementado tratamento
     */
    irc_client.addListener('names', function (canal, nicks) {
      console.log("\"Ouvindo\" o evento NAMES " + canal);
    });
    /**
     * Servidor IRC retorna topico do canal
     */
    irc_client.addListener('topic', function (canal, topic, nick, message) {
      console.log("\"Ouvindo\" o evento TOPIC " + canal);
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": message,
        "canal": canal,
        "topico": topic
      });
    });
    /* COMANDO 'RAW': EMITIDO SEMPRE QUE UMA MSG E RECEBIDA DO SERVIDOR IRC.
    ** ESTE LISTENER FICARÁ COMENTADO, POIS ELE "POLUI" MUITO O LOG DO 
    ** IRC-PROXY. DESCOMENTAR SEMPRE QUE ACHAR NECESSÁRIO. */
    
   /* irc_client.addListener('raw', function (message) {
      switch (message.command) {
        case 'PONG':
          
          break;
        case 'MOTD':
          
          break;
        case 'PONG':
          
          break;
        case 'PONG':
          
          break;
        case 'PONG':
          
          break;
        case 'PONG':
          
          break;
        case 'PONG':
          
          break;
        case 'PONG':
          
          break;
        case 'rpl_endofmotd':
          
          break;
        case 'NOTICE':
          console.log('Recebeu comando NOTICE');
          enviarParaCliente(id, {
            "timestamp": Date.now(),
            "nick": (message.prefix == message.server) ? servidor_irc : message.prefix,
            "msg": message.args[1]
          });
          break;
        default:
          console.log('Comando não identificado! ' + message.command);
          break;
      }
    });*/
    /**
     * Servidor IRC retorna o nick antigo e o novo nick do usuario que alterou seu nick
     */
    irc_client.addListener('nick', function (oldnick, newnick, channels, message) {
      console.log("\n\"Ouvindo\" o evento NICK.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": "\"" + oldnick + "\" alterou seu nick para \"" + newnick + "\"."
      });
    });

    /**
     * Servidor IRC retorna o usuario que saiu do servidor e o motivo
     */
    irc_client.addListener('part', function (channel, nick, motivo, message) {
      console.log("\n\"Ouvindo\" o evento PART.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": "Usuario" + nick + " deixou o canal! " + motivo + " - " + message + "."
      });
    });

    /**
     * Servidor IRC retorna o usuario que saiu do servidor e o motivo
     */
    irc_client.addListener('quit', function (nick, motivo, channels, message) {
      console.log("\n\"Ouvindo\" o evento QUIT.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": "Usuario" + nick + " se desconectou do servidor." + motivo + " - " + message
      });
    });

    /**
     * Servidor IRC retorna o usuario foi removido do servidor e o motivo
     */
    irc_client.addListener('kick', function (channel, nick, by, motivo, message) {
      console.log("\n\"Ouvindo\" o evento KICK.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": "Usuario" + nick + " foi removido do servidor pelo usuario " + by + " - " + motivo + " - " + message
      });
    });

    /**
     * Servidor IRC retorna quando um usuário for "morto"
     */
    irc_client.addListener('kill', function (nick, motivo, channels, message) {
      console.log("\n\"Ouvindo\" o evento KILL.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": "Usuario" + nick + " foi removido do servidor pelo usuario " + by + " - " + motivo + " - " + message
      });
    });

    /**
     * Servidor IRC retorna quando um usuário entra no canal
     */
    irc_client.addListener('join', function(channel, nick, message) {
        console.log("\n\"Ouvindo\" o evento JOIN.");
        enviarParaCliente(id, {
          "timestamp": Date.now(),
          "nick": servidor_irc,
          "msg": nick + ' se juntou ao canal ' + channel + "."
        });
    });

    /**
     * Servidor IRC retorna quando é enviado uma noticia para o canal
     */
    irc_client.addListener('notice', function(nick, to, text, message) {
      console.log("\n\"Ouvindo\" o evento NOTICE.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": (message.prefix == message.server) ? servidor_irc : message.prefix,
        "msg": text
      });
    });

    /**
     * Servidor IRC retorna quando é enviado uma mensagem para o canal
     */
    irc_client.addListener('message', function(nick, to, text, message) {
      console.log("\n\"Ouvindo\" o evento NOTICE.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": nick,
        "msg": text
      });
    });

    /**
     * Servidor IRC retorna quando é enviado uma noticia para o canal
     */
    irc_client.addListener('whois', function(info) {
      console.log("\n\"Ouvindo\" o evento WHOIS.");
      enviarParaCliente(id, {
        "timestamp": Date.now(),
        "nick": servidor_irc,
        "msg": info
      });
    });

    /**
     * Servidor IRC retorna quando é enviado um comando ping
     * Apenas o proxy responde PONG, e não o usuário (client) final
     */
    irc_client.addListener('ping', function(server) {
      console.log("\n\"Ouvindo\" o evento PING.");
      irc_client.say('pong');
    });

    proxies[id] = irc_client;
    irc_clients.push(irc_client);
  });
  
  receberDoCliente("gravar_mensagem", function (msg) {
    var irc_client = irc_clients.find(function(client){
      return client.nick == msg.nick
    });

    if (irc_client) {
      if (msg.msg.charAt(0) != '/') {
        irc_client.say(msg.canal, msg.msg);
        return;
      }

      var comando = msg.msg.split(" ")[0];

      switch (comando.toUpperCase()) {
        case '/MOTD':
          console.log("Enviando comando /motd para o Servidor IRC...");
          irc_client.send("motd");
          break;
        case '/NICK':
          console.log("Enviando comando /nick para o Servidor IRC...");
          irc_client.send("nick", msg.msg.split(" ")[1]);
          break;
        case '/JOIN':
          console.log("Enviando comando /join para o Servidor IRC...");
          irc_client.join(msg.msg.split(" ")[1], function(err) {
            if (err) {
              console.log("Ocorreu um erro durante a operação: " + err);
            } else {
              console.log(msg.nick + " se juntou ao canal ", msg.msg.split(" ")[1]);
            }
          });
          break;
        case '/PRIVMSG':
          console.log("Enviando comando /privmsg para o Servidor IRC...");
          var message = msg.msg.split(" ");
          message.shift();
          var target = message.shift();
          irc_client.ctcp(target, "privmsg", message.join(" "));
          break;
        case '/NOTICE':
          console.log("Enviando comando /notice para o Servidor IRC...");
          var message = msg.msg.split(" ");
          message.shift();
          var target = message.shift();
          irc_client.notice(target, message.join(" "));
          break;
        case '/WOIS':
          break;
        case '/LIST':
          break;
        case '/PART':
          console.log('Enviando comando /part para o servidor IRC..');
          irc_client.part(msg.canal, msg.mensagem, function(){

          });
          break;
        case '/ACTION':
          console.log('Enviando comando /action para o servidor IRC..');
          irc_client.action(msg.canal, msg.mensagem);
          break;
        default:
          var msgErroComando = "ATENÇÃO: comando \"" + comando + "\" desconhecido ou ainda não implementado!";
          console.log(msgErroComando);
          
          enviarParaCliente(msg.id, {
            "timestamp": Date.now(),
            "nick": msg.nick,
            "msg": msgErroComando
          });
          break;
      }
    } else {
      console.log('Proxy IRC não instanciado corretamente');
    }
  });
}

function receberDoCliente (canal, callback) {
  amqp_ch.assertQueue(canal, {durable: false});
  
  console.log(" [irc] Aguardando por mensagens no canal " + canal);
  
  amqp_ch.consume(canal, function(msg) {
    console.log(" [irc] Recebido %s", msg.content.toString());
    callback(JSON.parse(msg.content.toString()));
  }, {noAck: true});
}

function enviarParaCliente (id, msg) {  
  msg = new Buffer(JSON.stringify(msg));
  
  amqp_ch.assertQueue("user_" + id, {durable: false});
  amqp_ch.sendToQueue("user_" + id, msg);
  console.log(" [irc] Enviando para cliente ID " + id + ": " + msg);
}
