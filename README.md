[![node](https://img.shields.io/badge/node-%3E%3D%208.9.4-green.svg)](https://nodejs.org/en/)
[![npm](https://img.shields.io/npm/v/npm.svg)](https://www.npmjs.com)
[![npm](https://img.shields.io/badge/license-MIT-orange.svg)](https://opensource.org/licenses/MIT)

projeto 3 - grupo 4
Disciplina de Aplicações Distribuidas
---
Contribuíntes:
---
Wellington Alves - [wac.0013@gmail.com](https://gitlab.com/wac0013)<br>
Raquel Andrade - [rla.raquelandrade@gmail.com](https://gitlab.com/raquelAndr)<br>
Gleydson Silva - [gleydsoneps@gmail.com](https://gitlab.com/gleydsoneps) <br>
Jader Henrique - [jaderhfa96@gmail.com] (https://gitlab.com/jaderhfa) 