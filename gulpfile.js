var 
  gulp = require('gulp'),
  nodemon = require('gulp-nodemon');

gulp.task('dev-app', function (done) {
  var stream = nodemon({ 
    script: 'app.js', 
    ext: 'js',
    env: { 'NODE_ENV': 'development' },
    watch: ['app.js', './comandos/*'],
    execMap: {
      js: "node --inspect=2929"
    },
    done: done
  });

  stream
      .on('restart', function () {
        console.log('APP - restarted!')
      })
      .on('crash', function() {
        console.error('APP - Application has crashed!\n')
        stream.emit('restart', 10)  // restart the server in 10 seconds
      })
})

gulp.task('dev-irc', function (done) {
  var stream = nodemon({ 
    script: 'irc-proxy.js', 
    ext: 'js',
    env: { 'NODE_ENV': 'development' },
    watch: ['irc-proxy.js', './comandos/*'],
    execMap: {
      js: "node --inspect=5555"
    },
    done: done
  });

  stream
      .on('restart', function () {
        console.log('IRC - restarted!')
      })
      .on('crash', function() {
        console.error('IRC - Application has crashed!\n')
        stream.emit('restart', 10)  // restart the server in 10 seconds
      })
})

gulp.task('dev', ['dev-irc', 'dev-app']);
