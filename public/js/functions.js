$(function(){
  /**
    * Transforma data no formato timestamp para HH:MM:SS
    */
  function timestampToDate(timestamp) {
    var data = new Date(timestamp);
    var hora = data.getHours();
    var s_hora = hora < 10 ? "0" + hora : "" + hora;
    var minutos = data.getMinutes();
    var s_minutos = minutos < 10 ? "0" + minutos : "" + minutos;
    var segundos = data.getSeconds();
    var s_segundos = segundos < 10 ? "0" + segundos : "" + segundos;
    return s_hora + ":" + s_minutos + ":" + s_segundos;
  }

  Vue.component('v-mensagem', {
    props: {
      enviando: {
        type: Boolean
      },
      cor_usuario: {
        type: String,
        default: 'black'
      },
      hora: {
        type: [Number, String],
        default: ''
      },
      nick: {
        type: String
      }
    },
    template:
      '<div class="col-sm-12 message-main">' +
        '<div v-bind:class="{sender: this.enviando, receiver: !this.enviando}">' +
          '<div class="message-text">' +
            '<div v-if="!this.enviando" class="" style="' +
              'color: blue;' +
              'display:  flex;' +
              'line-height: 22px;' +
              'font-size:  12.5px;' +
              'font-weight: 600;' +
              'font-family:  \'Open Sans\', sans-serif;">' +
                '<span style="outline: none; white-space: nowrap;">{{this.nick}}</span>' +
            '</div>' +
            '<slot></slot>' +
          '</div>' +
          '<div class="message-time pull-right">' +
            '<span style="vertical-align: top; display: inline-block; margin-top: 1px">{{this.hora}}</span>' +
          '</div>' +
        '</div>' +
      '</div>'
  });

  var app = new Vue({
    el: '#mural',
    data: {
      nick: '',
      status: 'Offline',
      nicks:[{
        nick: this.nick,
        cor: ''
      }],
      mensagens:[]
    },
    methods: {
      isEnviando: function(mensagem) {
        return this.nick == mensagem.origem;
      },
      /**
       * Função para envio de mensagens
       */
      enviarMensagem: function() {
        this.submeterMensagem($('#mensagem')[0].value.trim());
        $('#mensagem').val('');
      },
     /**
      * Carrega as mensagens vindas do servidor,
      * busca novas mensagens a cada segundo
      */
      carregarMensagens: function(timestamp){
        var mensagem = "";
        var horario = "";
        var novo_timestamp = timestamp || "0";
        var self = this;

        $.get("obter_mensagem/" + timestamp, function(data, status) {
          if ( status == "success" ) {
            var linhas = data;

            for ( var i = linhas.length - 1; i >= 0; i-- ) {
              horario = timestampToDate(linhas[i].timestamp);
              mensagem =   "[" + horario + " - " + linhas[i].nick + "]: " + linhas[i].msg;
              novo_timestamp = linhas[i].timestamp;
              self.mensagens.push({
                id: i,
                origem: linhas[i].nick,
                cor_usuario: 'green',
                hora: horario,
                mensagem: linhas[i].msg
              });
              var container = self.$el.querySelector("#conversation");
              container.scrollTop = container.scrollHeight;
            }
          } else {
              alert("erro: " + status);
          }
        });

        t = setTimeout(function(){self.carregarMensagens(novo_timestamp)}, 1000);
      },
      /**
       * Submete mensagem para o servidor
       */
      submeterMensagem: function(mensagem){
        var msg = '{"timestamp":' + Date.now() + ',' + '"nick":"' + Cookies.get("nick") + '",' + '"msg":"' + mensagem + '"}';
      
        $.ajax({
          type: "post",
          url: "/gravar_mensagem",
          data: msg,
          success: 
          function(data,status){
            if (status == "success"){
                console.log('entrou no submete_mensagem -> /gravar_mensagem: success!');
            }else{
              alert("erro:" + status);
            }
          },
          contentType: "application/json",
          dataType: "json"
        });
      },
      /**
       * Trocando o modo do usuário conectado
       */
      trocarModo: function (elemento){
        var usuario = Cookies.get("nick");
        var args = $("#" + elemento).val();
        var comando = "mode/" + usuario + "/" + args;
      
        $.get(comando, function(data,status) {
          if ( status == "success" ) {
            alert(comando);
          }
        });
      },
      /**
       * Adicionando nick para lista de nicks presentes no canal
       */
      addNick: function(nick){
        if (this.nicks.indexOf(nick) > -1) {
          return
        }
        var hexadecimais = '0123456789ABCDEF';
        var cor = '#';
      
        // Pega um número aleatório no array acima e concatena à variavel cor
        for (var i = 0; i < 6; i++ ) {
          cor += hexadecimais[Math.floor(Math.random() * 16)];
        }

        this.nicks.push({nick: nick, cor: cor});
      }
    },
    created() {
      this.nick = Cookies.get("nick");
      $("#status").text(
        "Conectado - irc://" +
        Cookies.get("nick") + "@" +
        Cookies.get("servidor") + "/" +
        Cookies.get("canal")
      );
    },
    mounted() {
      this.carregarMensagens();
    },
  });

  $(".heading-compose").click(function() {
    $(".side-two").css({
      "left": "0"
    });
  });

  $(".newMessage-back").click(function() {
    $(".side-two").css({
      "left": "-100%"
    });
  });

});
